#! /usr/bin/env python

''' Setup script for Blackboard connector for polynum
'''

from setuptools import setup, find_packages

setup(name="polynum-blackboard",
      version='0.25.0',
      license="AGPLv3 or later",
      description="Allows sending documents from Polynum to the Blackboard e-Learning platform",
      url="http://dev.entrouvert.org/projects/polynum-blackboard/",
      author="Entr'ouvert",
      author_email="info@entrouvert.org",
      maintainer="Benjamin Dauvergne",
      maintainer_email="bdauvergne@entrouvert.com",
      packages=find_packages(),
      include_package_data=True,
      package_data={
          'polynum_blackboard': ['*.html','*.js'],
      },
      entry_points={
          'polynum_plugin': [
              'blackboard = polynum_blackboard.plugin:Plugin',
          ],
      },
      install_requires=[
          'requests<0.15.0',
      ],
)
