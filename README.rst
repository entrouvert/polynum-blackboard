Connecteur Blackboard/MyCouse pour Polynum
==========================================

Comment installer le connecteur
-------------------------------

Installer le connecteur dans le virtualenv où est installé polynum::

    $ pip install http://repos.entrouvert.org/polynum-blackboard.git/snapshot/polynum-blackboard-master.zip

Mettez à jour votre fichier `local_settings.py` comme suit::

    from polynum.settings import INSTALLED_APPS
    ROOT_URLCONF = 'polynum_blackboard.urls'
    INSTALLED_APPS = ('polynum_blackboard',) + INSTALLED_APPS

Clés de configuration spécifiques
---------------------------------

==================================== =================
Name                                 Value
==================================== =================
POLYNUM_BB_URL                       Base url of the web service, like https://mycourse.dauphine.fr/webapps/
POLYNUM_BB_ADMIN_LOGIN               login of the admin user
POLYNUM_BB_ADMIN_PASSWORD            password of the admin user
POLYNUM_BB_SECRET_KEY                shared secret key
POLYNUM_BB_COURSE_YEAR               2012-2013
POLYNUM_BB_DIPLOMA_DESIGNATION       le type d'entité correspondant à un diplome, ex.: diplome
POLYNUM_BB_UE_DESIGNATION            le type d'entité correspondante à une UE, ex.: element pedagogique
==================================== =================

Comment activer le panneau d'envoi à MyCourse ?
-----------------------------------------------

Pour cela il faut associer le tag « Diffusion vers MyCourse » pour un des
modes de diffusion gérer par la plateforme URL relative par rapport à la
plateforme, /admin/base/documentlicence/.
