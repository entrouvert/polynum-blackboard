from django.conf.urls import patterns, url


urlpatterns = patterns('polynum_blackboard.wizard',
    url(r'^request/(?P<pk>\d+)/edit/(?P<step>.+)/$', 'request_wizard_step', name='request_wizard_step'),
    url(r'^request/(?P<pk>\d+)/edit/$', 'request_wizard_step', name='request_wizard'),
)

urlpatterns += patterns('polynum_blackboard.views',
    url(r'^request/(?P<pk>\d+)/course-list/$', 'courses_list', name='courses_list'),
    url(r'^bb/create_course/', 'create_course', name='create_course'),
)

