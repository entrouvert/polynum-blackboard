# vim:spell:spelllang=fr
# -*- encoding: utf-8 -*-

import logging
import os
from optparse import make_option

from django.core.management.base import BaseCommand
from django.db import transaction

from ... import models, django_ws

class Command(BaseCommand):
    '''
       Envoyer les demandes blackboard
    '''
    can_import_django_settings = True
    requires_model_validation = True
    args = ''
    help = 'Envoyer les demandes blackboard'

    option_list = BaseCommand.option_list + (
            make_option("--send", action="store_true"),
            make_option("--list", action="store_true"),
            make_option("--delete", action="store", type="string"))

    @transaction.commit_on_success
    def handle(self, *args, **options):
        logger = logging.getLogger(__name__)
        bb_pushs = models.BlackBoardPush.objects.filter(sent=False, request__isnull=False)
        if options['delete']:
            qs = models.BlackBoardPush.objects.filter(id__in=options['delete'].split(','))
            for push in qs:
                print ' - deleted push request %s for document %s' % (push.id, push.request.id)
            qs.delete()
        if options['list']:
            for push in bb_pushs:
                if not push.request or not push.request.uploadfile:
                    continue
                print ' -', push.id, 'document', os.path.basename(push.request.uploadfile.name).encode('utf-8'), '(%s)' % \
                    push.request.id, 'de', push.request.user.display_name().encode('utf-8'), 'dans le cours', push.course_name.encode('utf-8'),
                if push.visible_to_students:
                    print '(visible aux étudiants)',
                print
        if options['send']:
            conn = django_ws.get_bb_conn()
            for push in bb_pushs:
                logger.info('sending request %s to mycourse', push.request.pk)
                logger.info('from user %s', push.request.user.display_name().encode('utf-8'))
                logger.info('into course code %s', push.course_code.encode('utf-8'))
                logger.info('into course name %s', push.course_name.encode('utf-8'))
                if push.visible_to_students:
                    logger.info('visible to students')
                try:
                    ok, result = conn.send_file(push.request.uploadfile, [push.course_code], push.visible_to_students)
                    push.sent = True
                    if ok:
                        logger.info('sending succeeded')
                        push.failure = ''
                    else:
                        push.failure = result
                        logger.error('sending failed: %r', result.encode('utf-8'))
                except Exception, e:
                    logger.exception('sending failed')
                    push.failure = unicode(e)
                push.sent = True
                push.save()
