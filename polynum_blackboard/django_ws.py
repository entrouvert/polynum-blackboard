from . import app_settings, ws

def get_bb_conn():
    url = app_settings.URL
    login = app_settings.ADMIN_LOGIN
    password = app_settings.ADMIN_PASSWORD
    secret_key = app_settings.SECRET_KEY
    return ws.BlackboardConnector(url, login, password, secret_key)
