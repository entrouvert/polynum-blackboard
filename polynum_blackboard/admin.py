from django.contrib import admin
from polynum.base.admin import site
from django.utils.translation import ugettext_lazy as _

import models

class BlackBoardPushAdmin(admin.ModelAdmin):
    actions = [ 'relaunch' ]
    readonly_fields = [ 'request', 'course_name', 'visible_to_students',
            'failure' ]
    list_display = [ 'created_on', 'request_id', 'course_code', 'course_name', 'visible_to_students', 'sent', 'failure' ]

    def request_id(self, instance):
        return instance.request and instance.request.id

    def relaunch(self, request, queryset):
        queryset.update(sent=False, failure='')
    relaunch.short_description = _("Relaunch selected pushes")

site.register(models.BlackBoardPush, BlackBoardPushAdmin)
