from django.db import models
from django.utils.translation import ugettext_lazy as _

from polynum.base.models import Request, DocumentLicence

import constant

class BlackBoardPush(models.Model):
    request = models.ForeignKey(Request, on_delete=models.SET_NULL, null=True, blank=True)
    course_code = models.CharField(max_length=128)
    course_name = models.CharField(max_length=128)
    visible_to_students = models.BooleanField(blank=True)
    sent = models.BooleanField(blank=True, default=False)
    failure = models.TextField(default='')
    created_on = models.DateTimeField(auto_now_add=True)
    modified  = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        s = u"push to blackboard request {request} into course {course_code}:{course_name}".format(request=self.request,
                **self.__dict__)
        if self.visible_to_students:
            s += u', set it visible to students'
        if self.sent:
            s += u', already sent'
        if self.failure:
            s += u', failed: {0}'.format(self.failure)
        return s


DocumentLicence.add_diffusion_tag(constant.MYCOURSE_TAG, _('Diffusion vers MyCourse'))
