from django.contrib.auth.decorators import login_required

from polynum.request import views as request_views

from . import app_settings, forms, views

named_new_request_forms = []

for step, form_class in request_views.named_new_request_forms:
    p = (step, form_class)
    if step == 'document_copyrights':
        p = (step, forms.MyCourseForm)
    named_new_request_forms.append(p)

class RequestWizardView(request_views.RequestWizardView):
    def get_context_data(self, form, **kwargs):
        context = super(RequestWizardView, self).get_context_data(form=form, **kwargs)
        polynum_request = self.get_object()
        context['create_course_form'] = forms.CreateCourseForm(request=self.request,
                initial={'entity': polynum_request.entity})
        context['courses'] = views.courses_ctx(self.request, polynum_request)
        context['course_year'] = app_settings.COURSE_YEAR
        return context

request_wizard_step = login_required(RequestWizardView.as_view(named_new_request_forms,
    done_step_name='finished', url_name='request_wizard_step'))
