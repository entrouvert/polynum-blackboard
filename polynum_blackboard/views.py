# -*- coding: utf-8 -*-
import json
import logging
import datetime

from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.template import RequestContext
from django.template.loader import render_to_string
from django import shortcuts
from django.forms.util import ErrorList
from django.forms.forms import NON_FIELD_ERRORS
from django.core.mail import send_mail
from django.conf import settings

from polynum.base.models import Request, Entity

import django_ws
from forms import CreateCourseForm
from . import app_settings

logger = logging.getLogger(__name__)

@require_POST
@login_required
def create_course(request):
    '''Ajax view'''
    form = CreateCourseForm(data=request.POST, request=request)
    ctx = { 'create_course_form': form }
    data = {
        "success": 0,
        "course_year": app_settings.COURSE_YEAR
    }
    try:
        document = Request.objects.get(id=request.POST.get('request-id'))
    except Request.DoesNotExist:
        document = None
    ctx['object'] = document
    if form.is_valid():
        conn = django_ws.get_bb_conn()
        entity = form.cleaned_data['entity']
        diploma = entity.parent_of_type('version diplome') or entity
        group = ''
        if form.cleaned_data.get('td_group'):
            group = '_Gr{0}'.format(form.cleaned_data['td_group'])
        year = app_settings.COURSE_YEAR.split('-')[0]
        course_code = u'{entity_code}_{year}{group}'.format(
                entity_code=entity.code.upper(),
                year=year,
                group=group)
        course_name = u'{diploma.name}_{scholar_year}_{entity.description}{group}_{sponsor_name}'.format(
                scholar_year=app_settings.COURSE_YEAR,
                diploma=diploma,
                entity=entity,
                group=group,
                sponsor_name=document.sponsor_name())
        logger.debug('trying to create course with code %r and name %r',
                course_code, course_name)
        try:
            userID = request.user.username
            mandatary = None
            if document.sponsor_username():
                mandatary = userID
                userID = document.sponsor_username()
            ok, result = conn.create_course(course_code,
                    course_name,
                    form.cleaned_data['category'],
                    form.cleaned_data['open_to_visitors'],
                    form.cleaned_data['subscription_policy'] != 'no',
                    form.cleaned_data['password'],
                    userID=userID,
                    mandatary=mandatary)
        except Exception:
            raise
        if ok:
            logger.info('user %s created course %r with label %r',
                    request.user.username.encode('utf-8'), 
                    course_code, course_name)
            notify_mailinglist = app_settings.COURSE_CREATE_NOTIFY_EMAIL
            if notify_mailinglist:
                ctx = form.cleaned_data.copy()
                ctx['course_code'] = course_code
                ctx['username'] = request.user.username
                ctx['display_name'] = request.user.display_name()
                ctx['open_to_visitors'] = 'oui' if ctx['open_to_visitors'] else 'non'
                ctx['subscription_policy'] = dict(form.fields['subscription_policy'].choices)[ctx['subscription_policy']]
                send_mail(u'Nouveau cours %s créé par %s' % (course_code,
                    request.user.username),
                    u'''\
Le nouveau cours %(course_code)s vient d'être créé par %(display_name)s (%(username)s)

Categorie: %(category)s
Ouvert aux visiteurs: %(open_to_visitors)s
Auto-enregistrement: %(subscription_policy)s
Mot de passe: %(password)s
''' % ctx,
                    settings.DEFAULT_FROM_EMAIL,
                    notify_mailinglist)
            data = {
                    'success': 1,
                    'entity_pk': form.cleaned_data['entity'].pk,
                    'course_code': course_code,
                    'description': course_name,
                   }
        else:
            logger.error('user %s failed to create course %r: %r',
                    request.user.username.encode('utf-8'), 
                    course_code, result)
            errors = form._errors.setdefault(NON_FIELD_ERRORS, ErrorList())
            errors.append(result)
    if not data['success']:
        data['html'] = render_to_string("_create_course.html", ctx,
                context_instance=RequestContext(request))
    return HttpResponse(json.dumps(data), mimetype="application/json")

@login_required
def courses_list(request, pk):
    '''Generate the list of courses with respect to Request object given by
       pk.
    '''
    polynum_request = shortcuts.get_object_or_404(Request, pk=pk)
    return shortcuts.render(request, "_select_course.html", { "courses":
        courses_ctx(request, polynum_request)})

def courses_ctx(request, polynum_request):
    '''Context building for the course selection template.'''
    ctx = {}
    conn = django_ws.get_bb_conn()
    ok, result = conn.get_course_by_owner(request.user.username)
    if ok:
        ctx['user_courses'] = result
    else:
        ctx['user_courses_error'] = result
    sponsor_username = polynum_request.sponsor_username()
    if sponsor_username and sponsor_username != request.user.username:
        ok, result = conn.get_course_by_owner(sponsor_username)
        if ok:
            ctx['sponsor_courses'] = result
        else:
            ctx['sponsor_courses_error'] = result
    if polynum_request.entity or 'ue' in request.GET:
        ue = request.GET.get('ue')
        possible_ue = None
        if ue:
            possible_ue = Entity.objects.filter(code=ue.lower())
            if possible_ue:
                possible_ue = possible_ue[0]
        if ue is None:
            possible_ue = polynum_request.entity
            if possible_ue.entity_type.name != app_settings.UE_DESIGNATION:
                possible_ue = None
        if possible_ue:
            ctx['ue'] = possible_ue.get_name()
            ue = possible_ue.code.upper()
            ok, result = conn.get_course_by_ue(ue)
            if ok:
                ctx['ue_courses'] = result
            else:
                ctx['ue_courses_error'] = result
    return ctx
