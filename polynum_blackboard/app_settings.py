# -*- coding: utf-8 -*-

import sys


class AppSettings(object):
    '''Thanks django-allauth'''
    __DEFAULTS = dict(
            URL = 'https://mycourse.dauphine.fr/webapps/',
            ADMIN_LOGIN = 'testbb',
            ADMIN_PASSWORD = '',
            SECRET_KEY = '',
            DIPLOMA_DESIGNATION = 'diplome',
            UE_DESIGNATION = 'element pedagogique',
            COURSE_CREATE_NOTIFY_EMAIL = [ ],
            RESUME_MESSAGE=u'Votre document sera en ligne dans 5 minutes, dans un menu nommé "Reprographie".',
    )

    def __init__(self, prefix):
        self.prefix = prefix

    @classmethod
    def get_default_course_year(cls):
        import datetime
        t = datetime.date.today()
        if t.month > 7:
            return '%s-%s' % (t.year, t.year+1)
        else:
            return '%s-%s' % (t.year-1, t.year)

    @property
    def settings(self):
        from django.conf import settings
        return settings

    @property
    def COURSE_YEAR(self):
        key = self.prefix+'COURSE_YEAR'
        if hasattr(self.settings, key):
            return getattr(self.settings, key)
        return self.get_default_course_year()

    def __getattr__(self, key):
        if key in self.__DEFAULTS:
            return getattr(self.settings,
                    self.prefix+key, self.__DEFAULTS[key])
        else:
            from django.core.exceptions import ImproperlyConfigured
            try:
                return getattr(self.settings, self.prefix+key)
            except AttributeError:
                raise ImproperlyConfigured('settings %s is missing' % self.prefix+key)


app_settings = AppSettings('POLYNUM_BB_')
app_settings.__name__ = __name__
app_settings.__file__ = __file__
sys.modules[__name__] = app_settings
