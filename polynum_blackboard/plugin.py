class Plugin(object):
    def get_applications(self):
        return ['polynum_blackboard']

    def get_urls(self):
        from . import urls
        return ((0, urls.urlpatterns),)

    def get_template_dirs(self):
        import os.path
        return [os.path.join(os.path.dirname(__file__), 'templates')]
